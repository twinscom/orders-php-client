<?php

declare(strict_types=1);

namespace twinscom\Orders\Tests;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use twinscom\GuzzleComponents\RetryDecider;
use twinscom\Orders\Client;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 *
 * @internal
 */
final class ClientTest extends TestCase
{
    public function testGuzzleInstantiation(): void
    {
        $baseUri = 'http://localhost/test/';
        $username = 'username';
        $password = 'password';

        $notifierClient = new Client([
            'baseUri' => $baseUri,
            'username' => $username,
            'password' => $password,
            'language' => 'ru',
        ]);

        $guzzleHttpClient = $notifierClient->getGuzzleHttpClient();

        self::assertInstanceOf(GuzzleHttpClient::class, $guzzleHttpClient);

        self::assertSame(
            $baseUri,
            (string) $guzzleHttpClient->getConfig('base_uri')
        );

        self::assertSame(
            [$username, $password],
            $guzzleHttpClient->getConfig('auth')
        );

        self::assertSame(
            'ru',
            $guzzleHttpClient->getConfig('headers')['Accept-Language']
        );

        self::assertContains(
            'Middleware::retry',
            $guzzleHttpClient->getConfig('handler')->__toString()
        );
    }

    public function testGetStatuses(): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(200, [], '[123]')],
                $container
            ),
        ]);

        $result = $notifierClient->getStatuses();

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame('GET', $request->getMethod());

        self::assertSame('statuses', $request->getUri()->getPath());

        self::assertSame(
            [123],
            $result
        );
    }

    public function testGetClientsWithoutParameters(): void
    {
        $this->getManyWithoutParameters('client');
    }

    public function testGetClientsWithParameters(): void
    {
        $this->getManyWithParameters('client');
    }

    public function testGetClientsResult(): void
    {
        $this->getManyResult('client');
    }

    public function testGetClient(): void
    {
        $this->getOne('client');
    }

    public function testUpdateClient(): void
    {
        $this->updateOne('client');
    }

    public function testCreateClient(): void
    {
        $this->createOne('client');
    }

    public function testGetOrdersWithoutParameters(): void
    {
        $this->getManyWithoutParameters('order');
    }

    public function testGetOrdersWithParameters(): void
    {
        $this->getManyWithParameters('order');
    }

    public function testGetOrdersResult(): void
    {
        $this->getManyResult('order');
    }

    public function testGetOrder(): void
    {
        $this->getOne('order');
    }

    public function testUpdateOrder(): void
    {
        $this->updateOne('order');
    }

    public function testCreateOrder(): void
    {
        $this->createOne('order');
    }

    public function testErrorMessageParsing(): void
    {
        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [
                    new Response(422, [], json_encode([
                        'message' => 'Response message',
                    ])),
                ]
            ),
        ]);

        $this->expectExceptionMessage('Response message');

        $notifierClient->getStatuses();
    }

    public function testRetryMechanism(): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [
                    new Response(500, [], json_encode([
                        'message' => 'You should not see it',
                    ])),
                    new Response(200, [], '[123]'),
                ],
                $container
            ),
        ]);

        $result = $notifierClient->getStatuses();

        $request = $container[1]['request'];

        assert($request instanceof Request);

        self::assertSame('GET', $request->getMethod());

        self::assertSame('statuses', $request->getUri()->getPath());

        self::assertSame(
            [123],
            $result
        );
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private function getGuzzleHttpClient(
        array $responses,
        array &$container = []
    ) {
        $mockHandler = new MockHandler($responses);
        $handlerStack = HandlerStack::create($mockHandler);

        $handlerStack->push(
            Middleware::retry(
                RetryDecider::make()
            )
        );

        $history = Middleware::history($container);
        $handlerStack->push($history);

        return new GuzzleHttpClient([
            'handler' => $handlerStack,
        ]);
    }

    private function getManyWithoutParameters(string $modelName): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(200, [], '[123]')],
                $container
            ),
        ]);

        $methodName = "get{$modelName}s";

        $notifierClient->{$methodName}();

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame('GET', $request->getMethod());

        self::assertSame("{$modelName}s", $request->getUri()->getPath());

        self::assertSame(
            http_build_query([
                'page' => 0,
                'per-page' => 10,
                'filter' => '{}',
            ]),
            $request->getUri()->getQuery()
        );
    }

    private function getManyWithParameters(string $modelName): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(200, [], '[123]')],
                $container
            ),
        ]);

        $methodName = "get{$modelName}s";

        $notifierClient->{$methodName}([
            'page' => 4,
            'perPage' => 8,
            'filter' => [
                'key' => '/значение/',
            ],
        ]);

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame(
            http_build_query([
                'page' => 4,
                'per-page' => 8,
                'filter' => '{"key":"/значение/"}',
            ]),
            $request->getUri()->getQuery()
        );
    }

    private function getManyResult(string $modelName): void
    {
        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [
                    new Response(
                        200,
                        [
                            'X-Total' => 1,
                            'X-Page' => 2,
                            'X-Per-Page' => 3,
                        ],
                        '[123]'
                    ),
                ]
            ),
        ]);

        $methodName = "get{$modelName}s";

        $result = $notifierClient->{$methodName}([
            'page' => 4,
            'perPage' => 8,
            'filter' => [
                'key' => 'value',
            ],
        ]);

        self::assertSame(
            [
                'models' => [123],
                'pagination' => [
                    'page' => 2,
                    'perPage' => 3,
                    'total' => 1,
                ],
            ],
            $result
        );
    }

    private function getOne(string $modelName): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(200, [], '[123]')],
                $container
            ),
        ]);

        $methodName = "get{$modelName}";

        $result = $notifierClient->{$methodName}('id');

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame('GET', $request->getMethod());

        self::assertSame("{$modelName}s/id", $request->getUri()->getPath());

        self::assertSame(
            [123],
            $result
        );
    }

    private function updateOne(string $modelName): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(200, [], '["result"]')],
                $container
            ),
        ]);

        $methodName = "update{$modelName}";

        $result = $notifierClient->{$methodName}(
            'id',
            ['key' => 'value']
        );

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame('PUT', $request->getMethod());

        self::assertSame("{$modelName}s/id", $request->getUri()->getPath());

        self::assertSame(
            ['key' => 'value'],
            json_decode($request->getBody()->getContents(), true)
        );

        self::assertSame(
            ['result'],
            $result
        );
    }

    private function createOne(string $modelName): void
    {
        $container = [];

        $notifierClient = new Client([
            'baseUri' => 'http://localhost/test/',
            'guzzleHttpClient' => $this->getGuzzleHttpClient(
                [new Response(201, [], '["result"]')],
                $container
            ),
        ]);

        $methodName = "create{$modelName}";

        $result = $notifierClient->{$methodName}(
            ['key' => 'value']
        );

        $request = $container[0]['request'];

        assert($request instanceof Request);

        self::assertSame('POST', $request->getMethod());

        self::assertSame("{$modelName}s", $request->getUri()->getPath());

        self::assertSame(
            ['key' => 'value'],
            json_decode($request->getBody()->getContents(), true)
        );

        self::assertSame(
            ['result'],
            $result
        );
    }
}
