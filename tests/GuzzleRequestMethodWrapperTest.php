<?php

declare(strict_types=1);

namespace twinscom\Orders\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use twinscom\Orders\Exception;
use twinscom\Orders\GuzzleRequestMethodWrapper;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 *
 * @internal
 */
final class GuzzleRequestMethodWrapperTest extends TestCase
{
    public function testInvocationAndReturnValue(): void
    {
        $guzzleStub = $this->createMock(Client::class);

        $guzzleStub
            ->method('request')
            ->willReturn('result');

        $requestMethodWrapper = GuzzleRequestMethodWrapper::get(
            $guzzleStub
        );

        $guzzleStub
            ->expects(self::once())
            ->method('request')
            ->with('GET', '/test', []);

        self::assertSame(
            'result',
            $requestMethodWrapper('GET', '/test', [])
        );
    }

    public function testRequestException(): void
    {
        $guzzleStub = $this->createMock(Client::class);

        $guzzleStub
            ->method('request')
            ->willThrowException(
                new RequestException(
                    'RequestException message',
                    new Request('GET', '/test'),
                    new Response(422, [], json_encode([
                        'message' => 'Response message',
                    ]))
                )
            );

        $requestMethodWrapper = GuzzleRequestMethodWrapper::get(
            $guzzleStub
        );

        $guzzleStub
            ->expects(self::once())
            ->method('request')
            ->with('GET', '/test', []);

        $this->expectExceptionMessage('Response message');

        $requestMethodWrapper('GET', '/test', []);
    }

    public function testResponselessRequestException(): void
    {
        $guzzleStub = $this->createMock(Client::class);

        $guzzleStub
            ->method('request')
            ->willThrowException(
                new RequestException(
                    'RequestException message',
                    new Request('GET', '/test')
                )
            );

        $requestMethodWrapper = GuzzleRequestMethodWrapper::get(
            $guzzleStub
        );

        $guzzleStub
            ->expects(self::once())
            ->method('request')
            ->with('GET', '/test', []);

        $this->expectExceptionMessage('RequestException message');

        $requestMethodWrapper('GET', '/test', []);
    }

    public function testExceptionErrors(): void
    {
        $guzzleStub = $this->createMock(Client::class);

        $guzzleStub
            ->method('request')
            ->willThrowException(
                new RequestException(
                    'RequestException message',
                    new Request('GET', '/test'),
                    new Response(422, [], json_encode([
                        'message' => 'Response message',
                        'errors' => [
                            'keyA' => 'valueA',
                            'keyB' => 'valueB',
                        ],
                    ]))
                )
            );

        $requestMethodWrapper = GuzzleRequestMethodWrapper::get($guzzleStub);

        try {
            $requestMethodWrapper('GET', '/test', []);
        } catch (Exception $e) {
            self::assertSame(
                [
                    'keyA' => 'valueA',
                    'keyB' => 'valueB',
                ],
                $e->getErrors()
            );
        }
    }
}
