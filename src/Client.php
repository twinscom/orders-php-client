<?php

declare(strict_types=1);

namespace twinscom\Orders;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use twinscom\GuzzleComponents\BackoffRetryDelay;
use twinscom\GuzzleComponents\RetryDecider;

class Client
{
    use ResponseBodyDecoder;

    /**
     * @var array
     */
    private $params;

    /**
     * @var callable
     */
    private $request;

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function __construct(array $params)
    {
        $handlerStack = HandlerStack::create();

        $handlerStack->push(
            Middleware::retry(
                RetryDecider::make($params['maxRetries'] ?? 3),
                BackoffRetryDelay::make(
                    $params['retryDelayCoefficient'] ?? 1000
                )
            ),
            'Middleware::retry'
        );

        $this->params = array_key_exists('guzzleHttpClient', $params)
            ? $params
            : array_replace(
                $params,
                [
                    'guzzleHttpClient' => new GuzzleHttpClient([
                        'base_uri' => $params['baseUri'],
                        'auth' => [$params['username'], $params['password']],
                        'headers' => ['Accept-Language' => $params['language']],
                        'handler' => $handlerStack,
                    ]),
                ]
            );

        $this->request = GuzzleRequestMethodWrapper::get(
            $this->params['guzzleHttpClient']
        );
    }

    public function getGuzzleHttpClient(): GuzzleHttpClient
    {
        return $this->params['guzzleHttpClient'];
    }

    public function getStatuses(): array
    {
        $request = $this->request;

        $response = $request(
            'GET',
            'statuses'
        );

        assert($response instanceof Response);

        return self::decodeResponseBody($response);
    }

    public function getClients(array $params = []): array
    {
        return $this->getMany('client', $params);
    }

    public function getClient(string $id): array
    {
        return $this->getOne('client', $id);
    }

    public function updateClient(string $id, array $existentClient): array
    {
        return $this->updateOne('client', $id, $existentClient);
    }

    public function createClient(array $newClient): array
    {
        return $this->createOne('client', $newClient);
    }

    public function getOrders(array $params = []): array
    {
        return $this->getMany('order', $params);
    }

    public function getOrder(string $id): array
    {
        return $this->getOne('order', $id);
    }

    public function updateOrder(string $id, array $existentOrder): array
    {
        return $this->updateOne('order', $id, $existentOrder);
    }

    public function createOrder(array $newOrder): array
    {
        return $this->createOne('order', $newOrder);
    }

    private function encodeJson($value): string
    {
        return json_encode(
            $value,
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );
    }

    private function getMany(string $modelName, array $params = []): array
    {
        $defaultedParams = array_replace(
            [
                'page' => 0,
                'perPage' => 10,
                'filter' => new \stdClass(),
            ],
            $params
        );

        [
            'page' => $page,
            'perPage' => $perPage,
            'filter' => $filter,
        ] = $defaultedParams;

        $request = $this->request;

        $response = $request(
            'GET',
            "{$modelName}s",
            [
                'query' => [
                    'page' => $page,
                    'per-page' => $perPage,
                    'filter' => $this->encodeJson($filter),
                ],
            ]
        );

        assert($response instanceof Response);

        return [
            'models' => json_decode((string) $response->getBody(), true),
            'pagination' => [
                'page' => (int) $response->getHeaderLine('X-Page'),
                'perPage' => (int) $response->getHeaderLine('X-Per-Page'),
                'total' => (int) $response->getHeaderLine('X-Total'),
            ],
        ];
    }

    private function getOne(string $modelName, string $id): array
    {
        $request = $this->request;

        $response = $request(
            'GET',
            "{$modelName}s/{$id}"
        );

        assert($response instanceof Response);

        return self::decodeResponseBody($response);
    }

    private function updateOne(
        string $modelName,
        string $id,
        array $existentModel
    ): array {
        $request = $this->request;

        $response = $request(
            'PUT',
            "{$modelName}s/{$id}",
            [
                'json' => $existentModel,
            ]
        );

        assert($response instanceof Response);

        return self::decodeResponseBody($response);
    }

    private function createOne(
        string $modelName,
        array $existentModel
    ): array {
        $request = $this->request;

        $response = $request(
            'POST',
            "{$modelName}s",
            [
                'json' => $existentModel,
            ]
        );

        assert($response instanceof Response);

        return self::decodeResponseBody($response);
    }
}
