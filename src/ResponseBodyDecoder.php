<?php

declare(strict_types=1);

namespace twinscom\Orders;

use GuzzleHttp\Psr7\Response;

trait ResponseBodyDecoder
{
    private static function decodeResponseBody(Response $response): array
    {
        return json_decode((string) $response->getBody(), true);
    }
}
