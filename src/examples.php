<?php

declare(strict_types=1);

use twinscom\Orders\Client;

require dirname(__DIR__) . '/vendor/autoload.php';

$ordersClient = new Client([
    'baseUri' => 'http://localhost/',
    'username' => '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
    'password' => 'password',
    'language' => 'ru',
]);

$ordersClient->getStatuses();

$ordersClient->getClients();

$ordersClient->getClients([
    'page' => 0,
    'perPage' => 10,
    'filter' => [
        'name' => 'ArmiT',
        'phone' => '+79999999999',
        'discount' => '123',
    ],
]);

$ordersClient->getClient('123');

$ordersClient->updateClient('123', [
    'name' => 'ArmiT',
    'phone' => '+79999999999',
    'discount' => '123',
]);

$ordersClient->getOrders();

$ordersClient->getOrders([
    'page' => 0,
    'perPage' => 10,
    'filter' => [
        'statusId' => 'new',
        'clientId' => '123',
    ],
]);

$ordersClient->getOrder('9bbf9c49-1db1-418f-a32f-f5a06c5ba8c8');

$ordersClient->updateOrder('9bbf9c49-1db1-418f-a32f-f5a06c5ba8c8', [
    'description' => 'Order description.',
    'statusId' => 'inProgress',
    'taskId' => 123,
    'address' => 'Order address.',
]);
