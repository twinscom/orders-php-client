<?php

declare(strict_types=1);

namespace twinscom\Orders;

class Exception extends \RuntimeException
{
    /**
     * @var array
     */
    private $errors = [];

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }
}
