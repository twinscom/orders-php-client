<?php

declare(strict_types=1);

namespace twinscom\Orders;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class GuzzleRequestMethodWrapper
{
    use ResponseBodyDecoder;

    public static function get(Client $guzzle): callable
    {
        return static function (...$args) use ($guzzle) {
            try {
                return $guzzle->request(...$args);
            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $response = self::decodeResponseBody(
                        $e->getResponse()
                    );

                    $newException = new Exception($response['message']);

                    if (array_key_exists('errors', $response)) {
                        $newException->setErrors($response['errors']);
                    }

                    throw $newException;
                }

                throw $e;
            }
        };
    }
}
