# orders-php-client

The reference implementation of a PHP client for orders

[![build status](https://gitlab.com/twinscom/orders-php-client/badges/master/build.svg)](https://gitlab.com/twinscom/orders-php-client/builds)
[![coverage report](https://gitlab.com/twinscom/orders-php-client/badges/master/coverage.svg)](https://gitlab.com/twinscom/orders-php-client/builds)

## Production

### Install

Add the repository to the "repositories" array in your `composer.json`:

```json
{
    "type": "vcs",
    "url": "git@gitlab.com:twinscom/orders-php-client.git"
}
```

Then install the package:

```sh
composer require twinscom/orders-php-client
```

### Use

[src/examples.php](src/examples.php)

## Development

### Install

```sh
docker-compose run --rm php composer install
```

### Test

```sh
docker-compose run --rm php bin/test.sh
```

### Fix the code with PHP-CS-Fixer

```sh
docker-compose run --rm php bin/php-cs-fixer/fix.sh
```

## License

[MIT](LICENSE)
